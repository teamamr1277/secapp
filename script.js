const playListContainerTag = document.getElementsByClassName("playListContainer")[0];

const audioTag = document.getElementsByClassName("audioTag")[0];

const currentPlayedTime = document.getElementsByClassName("currentAndTotalTime")[0];

const currentBar = document.getElementById("currentBar");

const icon = document.getElementsByClassName("icon")[0];

const channalList = document.getElementsByClassName("channalList")[0];
//radio source input
const tracks = [
{
trackId:"https://edge.mixlr.com/channel/nmtev",
img: "https://www.thitsarparamisociety.com/wp-content/uploads/2015/11/LOGO-copy.png",
title:"Thitsar Parami FM"
},

{
trackId:"https://listen.radioking.com/radio/354898/stream/404155",
img: "https://i1.sndcdn.com/artworks-000031962429-acuozb-t500x500.jpg",
title:"Shwe FM"
},


{
trackId:"http://159.65.15.33:8000/stream?type=http",
img: "https://dpi4fupzvxbqq.cloudfront.net/rfm/uploads/rlogo/300/300_1528881964.jpg",
title:"Cherry FM"
},
{
trackId:"https://s2.radio.co/s969e6c4b7/listen",
img: "https://mytuner.global.ssl.fastly.net/media/tvos_radios/7tvbk3tkszep.png",
title:"Padamyar FM"
},

{
trackId:"https://streamingv2.shoutcast.com/myanmar-bagan-fm",
img: "https://ws.shoutcast.com/images/contacts/6/6813/681373ae-eac0-4eaa-a843-a2445b6f0060/radios/56415c8c-d648-4c04-a22b-1751b1309e12/56415c8c-d648-4c04-a22b-1751b1309e12.png",
title:"Bagan FM"
},


{
trackId:"http://stream0.radiostyle.ru:8000/voiceofburma",
img: "https://dbs.radioline.fr/pictures/radio_321ee5b7527af57736a81fd778c3661b/logo200.jpg?size=600",
title:"VOB FM"
},
];
		//end of radio source

//get player buttons
const playBtn = document.getElementsByClassName("play")[0];
const pauseBtn = document.getElementsByClassName("pause")[0];
const nextBtn = document.getElementsByClassName("next")[0];
const preBtn = document.getElementsByClassName("pre")[0];
		//end of player buttons

//playtist making
for(i = 0; i < tracks.length; i++){	
	const imageSrc = tracks[i].img;
	const fmTitle = tracks[i].title;
	const radioIn = i;
	
	const trackTag = document.createElement("div");
	trackTag.addEventListener("click", ()=>{
					channalList.style.display = "block";
					playListContainerTag.style.display = "none";
						fmIndex = radioIn;
						playFm()
	});
	trackTag.classList.add("trackItems");
	
	channalList.addEventListener("click", ()=>{
					channalList.style.display = "none";
					playListContainerTag.style.display = "flex";
	})
	let trackImg = document.createElement("img")
	trackImg.classList.add("trackImage");
	trackImg.src= imageSrc;
	let title = (i + 1).toString() +". " + fmTitle;
	trackTag.append(trackImg, title);
	playListContainerTag.append(trackTag);

};
		// end of playlist making

//player buttons function
let fmIndex = 0;
let isPlaying = false;
playBtn.addEventListener("click", ()=>{
	if(audioTag.currentTime === 0){
	playFm();
	};
	audioTag.play();
	isPlaying = true;
	playAndPause();	
});

pauseBtn.addEventListener("click", () => {
	audioTag.pause();
	isPlaying = false;
	playAndPause();
});

nextBtn.addEventListener("click", () => {
 	if(fmIndex === tracks.length - 1){
 	return};
	fmIndex +=1;
	playFm();
});

preBtn.addEventListener("click", () => {
 	if(fmIndex === 0){
 	return};
	fmIndex -=1;
	playFm();
});
	// end of player buttons function

// update play and pause display
const playAndPause = ()=> {
if (isPlaying){
	playBtn.style.display = "none";
	pauseBtn.style.display = "inline";
}else{
	playBtn.style.display = "inline";
	pauseBtn.style.display = "none";
};
};
		//end of play.pause display

//play FM function
let playFm = ()=>{
	let fmName = tracks[fmIndex].title;
	let iconImg = tracks[fmIndex].img;
	let fmSrc = tracks[fmIndex].trackId;
	let playing= document.getElementsByClassName("playing")[0];
	playing.textContent= fmName;
 	icon.src = iconImg;
 	audioTag.src = fmSrc;

 	audioTag.play();
 	isPlaying = true;
 	playAndPause();
};
		// end of play FM
		



//current time update
audioTag.addEventListener("timeupdate", ()=> {
	 let playedTime = Math.floor(audioTag.currentTime);
	let playedMin = Math.floor(playedTime/60);
	let playedSec = playedTime%60;
	
	let minutes = playedMin <10 ? "0"+ playedMin : playedMin;
	let seconds = playedSec < 10 ? "0" + playedSec : playedSec;
 	let currentPlayed = minutes + ":" + seconds;
	if(playedSec < 1){
	currentPlayedTime.textContent = "loading....";
	}else{
	currentPlayedTime.textContent = currentPlayed;
	currentBar.style.width=playedTime.toString() + "px";}
 });
 		//end of current time update
